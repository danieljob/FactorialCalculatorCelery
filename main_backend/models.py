from flask_sqlalchemy import SQLAlchemy
from datetime import datetime
from sqlalchemy.types import LargeBinary
import pickle

db = SQLAlchemy()
#The flask_sqlalchemy module does not have to be initialized with the app right away
class FactorialModel(db.Model):
    __tablename__ = 'celery_taskmeta'

    id = db.Column(db.Integer, primary_key=True)
    task_id= db.Column(db.String())
    status= db.Column(db.String())
    result = db.Column(LargeBinary)
    date_done = db.Column(db.DateTime(),default=datetime.utcnow)

    def __init__(self, number:int ,factorial:int):
        self.number_input = number
        self.factorial = factorial
        

    def __repr__(self):
        return f"<factorial {self.number}>"

def getHistoryRecords()-> dict:
    """Queries the celery_taskmeta table in order to get all the previous 'task'

    Returns:
        dict: dictionay composed by id,uuid,status,result,date_done
    """
    resultset=FactorialModel.query.all()
    results = [
            {
                "id": factorial.id,
                "uuid": factorial.task_id,
                "status": factorial.status,
               # "result": factorial.result.decode("utf-8",errors="ignore"),
               # "result": loads(factorial.result),
                "result": pickle.loads(factorial.result), #celery use pickle by default
                "date_done": factorial.date_done
            } for factorial in resultset]
    return results