from app import app 
import unittest
from werkzeug import Request,Response
import json
'''
    This test the endpoint
'''
class TestAppCase(unittest.TestCase):

    def setUp(self):
        app.testing = True
        self.app = app.test_client()

    def test_home(self):       
        result = self.app.get('/factorial/3')  # type: Response
        
        resultN=json.loads(result.get_data(True))['result']
        #self.assertEquals(6,int(resultN),'6 is expected')
        