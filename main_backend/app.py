from flask import Flask,request
from markupsafe import escape
from flask import jsonify

from werkzeug import Request,Response
from flask_cors import CORS
from flask_celery import make_celery

import os
from os.path import join, dirname
from dotenv import load_dotenv
from os import environ 
dotenv_path = join(dirname(__file__), '.env')
load_dotenv(dotenv_path)
from logging.config import dictConfig

from factorialService.factorialclient import FactorialClient
#import 
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from models import db,FactorialModel,getHistoryRecords
import traceback
from factorial import calculate
dictConfig({
    'version': 1,
    'formatters': {'default': {
        'format': '[%(asctime)s] %(levelname)s in %(module)s: %(message)s',
    }},
    'handlers': {'wsgi': {
        'class': 'logging.StreamHandler',
        'stream': 'ext://flask.logging.wsgi_errors_stream',
        'formatter': 'default'
    }},
    'root': {
        'level': 'INFO',
        'handlers': ['wsgi']
    }
})

app = Flask(__name__)	
CORS(app, resources={r'/*': {'origins': '*'}})




app.config['CELERY_BROKER_URL']='amqp://guest@rabbit:5672//'
# I use result_backend insted of CELERY_RESULT_BACKEND because it will be deprecated in version 6.
app.config['result_backend']='db+postgresql://postgres:123456@db:5432/factorial_db' 
# We call our function
celery= make_celery(app)
# DATABSE
app.config['SQLALCHEMY_DATABASE_URI'] = "postgresql://postgres:123456@db:5432/factorial_db"
migrate = Migrate(app, db)
db.init_app(app)
# DATABASE END


#--        


@app.route('/')
def home():
    return 'welcome' 

@app.route('/factorial/<number>')
def factorial_start(number):
    number=number.strip()
    if not number.isdigit():
        if number.startswith('-'):
            errorMessage='Negative value invalid'+number
        else:
            errorMessage='No numeric value:'+number

        return {
            'message' : 'No numeric value:'+number,
            'task':{
                'id' : '',
                'status' : ''
            }
        }, 400
    number=int(number)
    if number<0:
        return {
            'message' : 'Invalid input:'+number,
            'task':{
                'id' : '',
                'status' : ''
            }
        },400
    task = factorial.delay(int(number)) # type: celery.result.AsyncResult 
    app.logger.info('The number received is %s ', number)
    print((task.id))
    return {
        'message' : 'I send an async request',
        'task':{
              'id' : task.id,
              'status' : task.state
        }
    }

@celery.task(name='app.factorial')
def factorial(n:int):
    #return 'hello'
    return str(calculate(n))


@app.route('/processs/status/<task_id>')
def getprocessStatus(task_id):
    """receives the task uuid and query its state and result

    Args:
        task_id (str): Id in uuid format 

    Returns:
        dict: [description]
    """
    task = factorial.AsyncResult(task_id)    
    print((task.id))
    return {
        'result' : task.info,
        'task_id' : task.id,
        'status' : task.state
    }    

@app.route('/factorial-history/',methods=['GET'])
def getHistory():
    results = getHistoryRecords()
    print(results)            
    return jsonify(results)    


if __name__ == '__main__':	
	#app.run()            
    app.run(host='0.0.0.0')