import unittest
import factorial

class TestFactorial(unittest.TestCase):
    """Test case for the factorial class

    Args:
        unittest ([type]): [description]
    """

    def test_factorial(self):
        self.assertEqual(factorial.calculate(1), 1, "Should be 1")
        self.assertEqual(factorial.calculate(2), 2, "Should be 2")
        self.assertEqual(factorial.calculate(3), 6, "Should be 6")


if __name__ == '__main__':
    unittest.main()