#from typing import TypeVar

def calculate(n:int)->int:
    """This Method calculates the factorial of a given number

    Args:
        n (int): input number

    Returns:
        int: factorial value of n
    """
    if n==0 or n==1:
        return 1
    return n*calculate(n-1)

