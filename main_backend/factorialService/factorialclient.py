import http.client
import json
class FactorialClientResponse:
    """ 
    Model class Response from the Factorial Service
    """
    def __init__(self,receivedNumber:int,result:int):
        super().__init__()
        self.receivedNumber=receivedNumber
        self.result=result
        

class FactorialClient:
    """Client to send the n value and get its factorial number
    """
    
    
    def __init__ (self,server):
        #self.conn = http.client.HTTPSConnection(server)
        self.conn = http.client.HTTPConnection(server)

   

    def getFactorial(self,n:int)->FactorialClientResponse:
        """Consumes the third party service 'Factorial Service' and process the response

        Args:
            n (int): Given input value to calculate its factorial value

        Returns:
            FactorialClientResponse: Modeled Response from the ws
        """
    
    
        self.conn.request('GET','/factorial/{number}'.format(number=n))
        response=self.conn.getresponse()
        
        data = response.read()
        data=data.decode('utf-8')
        if(response.status!=200):
            raise Exception('Error while consuming external service'+data)
        # if the code is 200 then:
        dictResponse=json.loads(data)
        return FactorialClientResponse(dictResponse['receivedNumber'],dictResponse['result'])


