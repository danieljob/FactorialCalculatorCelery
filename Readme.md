# Calculadora Factorial

Este ejercicio busca simular el manejo de operaciones costosas en una aplicación.

A través de una interfaz, el usuario ingresará un número al cual se le desea conocer su factorial. El cálculo del resultado se realizará en un servicio adicional de forma asíncrona, al cual se comunicará el servicio principal.

En dicha interfaz también se podrán ver las consultas anteriores.

La interacción/diseño, arquitectura de los servicios y coordinación entre front/back/worker/BD es propuesta por el desarrollador.

El flujo de desarrollo, documentación y pruebas de los diferentes componentes se deja al criterio del desarrollador.

La aplicación debe de ser desarrollada en python con el framework y librerías que se prefiera. Se sugiere el uso de flask, django o celery

El uso adicional de otra herramienta es libre. Se entregará en un repositorio público en control de versiones, con instrucciones de instalación y levantamiento.



# Design

We have three entities:

- Frontend
- Main Backend
- Aditional/third party Service (worker with Celery)

![image info](./design.png)


# Selected tools/framworks

## Frontend

I choose Vue because I already know it
- Vue
- Javascript
- Boostrap  

## Backend
I choose Flask, SQLAlchemy

## Worker
Celery

## Database.

- Postgres.


# API

# Deploy the app

To deploy the app is enough to run the next command


~~~bash
docker-compose -f docker-compose.dev.frontend.yml up
~~~

Make sure you have docker and docker-compose installed.

## Review deployed app
After you have run the previous you can open the following links in your browser to review the app.

### Frontend app (Entry point)

The entry point to start reviewing the application is the next URL.

http://localhost:8080/


### Backend

Here you can consume directly the backend without the frontend app.

http://localhost:5000/factorial/1




# Test
For simplicity I just implement unit test

Firts go inside main_backend, once that you are in that folder just run(make sure you have make command installed):

~~~bash
# this runs python unit test
make test
~~~

## Test Frontend

go into frontend and run (you must install the packages):
~~~bash
#this  run the frontend unit test
npm run test:unit
~~~
### Test frontend in docker

For the sake of reviewin the project I create a special docker to run the test , in one single line:

~~~bash
docker build  -t frontend-test:latest .  -f Dockerfile.test  && docker run --rm -it  frontend-test:lates
~~~

forcing not to use cache
~~~bash
docker build  --no-cache -t frontend-test:latest .  -f Dockerfile.test  && docker run --rm -it  frontend-test:lates
~~~

# Decision notes:

- I decide to use the celery table that generates in the postgres database to simplify the database


# TODO things

- Input validation in frontend is neccesary or just allow number and positive values but I leave it to check the response ❌

- Secure Main Backend  ❌

- Integration test ❌

# Images
![image info](./pictures/view.png)



# Notes

The frontend works with both Ngix server and development server "npm run serve" , the given above command starts the app  by running the dev server.

Ports 8080,5000 ,5672,15672 shouldn't be used in the host machine.


# About me

You can know more about me at: https://danielhernandez.dev/