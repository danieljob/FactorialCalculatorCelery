import MutationObserver from 'mutation-observer'
global.MutationObserver = MutationObserver 

import { render, screen,fireEvent } from '@testing-library/vue'
import HistoricalFactorialResult from '@/components/HistoricalFactorialResult.vue'
//import '@testing-library/jest-dom'


test('increments value on click', async () => {
  // The render method returns a collection of utilities to query your component.
  const renComponent = render(HistoricalFactorialResult,{
    props:{ resultSet:[
      {
        date_done:'',
        result:'3',
        status:'SUCCESS'
      },
      {
        date_done:'',
        result:'3',
        status:'SUCCESS'
      },
      {
        date_done:'',
        result:'3',
        status:'SUCCESS'
      }
    ]
  }})
  //option 1
  // const tr=renComponent.container.querySelectorAll('tr')
  // console.log(tr.length)

  //option 2
  const rows=await renComponent.findAllByRole('row')
  // console.log("rows:"+Array.from(rows).length)
 
  // console.log(Array.from(tr).length)
  expect(rows).toHaveLength(4)

})