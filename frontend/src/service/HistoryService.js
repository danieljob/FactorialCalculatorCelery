import axios from "axios";

import Toastify from "toastify-js";

export default function getHistory() {
    const server = process.env.VUE_APP_API_URL;
  return axios
    .get(server + "/factorial-history")
    .then((res) => {
      Toastify({
        text: `History received!`,
        duration: 3000,
        className: "warning",
      }).showToast();
      return res.data;
    })
    .catch((error) => {
      console.log(error);
      Toastify({
        text: "Error while getting result!",
        duration: 3000,
        backgroundColor: "linear-gradient(to right, #DF390C, #96c93d)",
      }).showToast();
    });
}
