
import Vue from 'vue'
import { BootstrapVue } from 'bootstrap-vue'
import App from './App.vue'

// Import Bootstrap an BootstrapVue CSS files (order is important)
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
Vue.use(BootstrapVue)
import "toastify-js/src/toastify.css"



Vue.config.productionTip = false

new Vue({
  render: h => h(App),
}).$mount('#app')
